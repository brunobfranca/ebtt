package br.tatuapu.model;

public class Salario2017 implements SalarioAno {

	private double salario;
	private double teto;
	private boolean regime;

	public Salario2017(double salario,double teto,boolean regime){
		this.salario=salario;
		this.teto=teto;
		this.regime = regime;
	}

	@Override
	public double calculo() {
		double variavel=0;
		if(salario <= teto){
			variavel = 0.11*salario;

		}else{
			if(!regime){
				double variavel2;
				variavel = 0.11*teto;
				variavel2 = (0.14*(salario-teto))+variavel;
				variavel= variavel2;
			}else{
				variavel = 0.11 * teto;
			}
		}
		return variavel;
	}

}
