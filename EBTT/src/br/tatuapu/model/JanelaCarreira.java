package br.tatuapu.model;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import org.omg.PortableServer.POAPackage.AdapterAlreadyExists;


public class JanelaCarreira extends JFrame{

	JComboBox<String> boxCarreiras;
	JComboBox<String> boxTitulacao;
	JComboBox<String> boxHoras;
	private List<Carreira> lista;
	private double constante2017= 5531.31;
	private double constante2019= 5953.77;
	private double constante2020= 6203.83;
	private String resultado;
	private double previdencia2017;
	private double previdencia2019;
	private double previdencia2020;
	DecimalFormat df;
	private JLabel lResultado;
	private JLabel lPrevidencia;
	private JCheckBox cbCarreira;

	public JanelaCarreira() {

		super ("Previdencia EBTT");
		setSize(1000,400);
		setLocationRelativeTo(null);
		JPanel painel = new JPanel(new BorderLayout());
		JPanel painelCentral = new JPanel(new BorderLayout());
		JPanel painelSuperior = new JPanel(new GridLayout(1,1,0,0));
		
		JMenuBar menu = new JMenuBar();
		JMenu menuAjuda = new JMenu("Ajuda");
		JMenuItem menuSair = new JMenuItem("Sair");
	
		JMenuItem mSobre = new JMenuItem("Sobre");
		JMenuItem mInformacao = new JMenuItem("Informações");
		
		cbCarreira = new JCheckBox("Ingresso apos 03/2013");
		
		
		
		menu.add(menuAjuda);
		menu.add(menuSair);
		menuAjuda.add(mSobre);
		menuAjuda.add(mInformacao);

		painelSuperior.add(menu);
		painelSuperior.add(cbCarreira);
		
		JLabel lCarreiras = new JLabel("Escolha uma Classe/Nível: ");
		painelSuperior.add(lCarreiras);

		boxCarreiras = new JComboBox<>();
		painelSuperior.add(boxCarreiras);
		preencheComboBoxCarreiras();


		JLabel lHoras= new JLabel("Escolha uma Carga Horaria: ");
		painelSuperior.add(lHoras);

		boxHoras = new JComboBox<>();
		painelSuperior.add(boxHoras);
		preencheComboBoxHoras();



		JLabel lTitulacao = new JLabel("Escolha uma Titulação: ");
		lResultado = new JLabel();
		lPrevidencia = new JLabel();
		painelSuperior.add(lTitulacao);

		boxTitulacao = new JComboBox<>();
		painelSuperior.add(boxTitulacao);
		painelSuperior.add(lResultado);
		preencheComboBoxTitulacao();



		JButton bConsultar = new JButton("Consultar");
		painelSuperior.add(bConsultar);
		

		painel.add("North",painelSuperior);
		painelCentral.add("Center",lResultado);
		painelCentral.add("North",lPrevidencia);
		painel.add(painelCentral);
		setContentPane(painel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		bConsultar.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent evt) {
				
					calculo();

			}		
		});
		mSobre.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "Este programa foi desenvolvido por Bruno Barbosa Franca. Tem por função calcular e mostrar as previdencias de carreiras no instituto!");
				
			}
		});
		mInformacao.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "A aplicação a seguir utiliza dados fornecidos PEC 805.Entretanto os dados podem ser alterados na proposta apos a finalizacao da aplicacao");
				
			}
		});
		menuSair.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
		});

	}

	public void preencheComboBoxCarreiras() {

		boxCarreiras.addItem("D I - 1");
		boxCarreiras.addItem("D I - 2");
		boxCarreiras.addItem("D II - 1");
		boxCarreiras.addItem("D II - 2");
		boxCarreiras.addItem("D III - 1");
		boxCarreiras.addItem("D III - 2");
		boxCarreiras.addItem("D III - 3");
		boxCarreiras.addItem("D III - 4");
		boxCarreiras.addItem("D IV - 1");
		boxCarreiras.addItem("D IV - 2");
		boxCarreiras.addItem("D IV - 3");
		boxCarreiras.addItem("D IV - 4");
		boxCarreiras.addItem("TITULAR");


	}

	public void preencheComboBoxHoras() {

		boxHoras.addItem("20H");
		boxHoras.addItem("40H");
		boxHoras.addItem("DE");

	}

	public void preencheComboBoxTitulacao() {

		boxTitulacao.addItem("Graduação");
		boxTitulacao.addItem("Aperfeiçoamento");
		boxTitulacao.addItem("Especialização ou Graduação + RSC I");
		boxTitulacao.addItem("Mestrado ou Especialização + RSC II");
		boxTitulacao.addItem("Doutorado ou Mestrado + RSC III");


	}
	public void calculo(){

		String classe;
		String tipo;
		String titulacao;

		ArrayList <Carreira> dadosEncontrados = new ArrayList<Carreira>();

		classe = boxCarreiras.getSelectedItem().toString();


		tipo = boxHoras.getSelectedItem().toString();


		titulacao = boxTitulacao.getSelectedItem().toString();

		df = new DecimalFormat("R$ 0.000");

		dadosEncontrados = encontraCarreira(classe, tipo, titulacao);				

		DecimalFormat df2 = new DecimalFormat("0.000%");			

		previdencia2017 = CalculadoraDeSalario.calculaPrevidencia(new  Salario2017(dadosEncontrados.get(0).getSalario(),constante2017,cbCarreira.isSelected()));
		previdencia2019 = CalculadoraDeSalario.calculaPrevidencia(new  Salario2017(dadosEncontrados.get(1).getSalario(),constante2019,cbCarreira.isSelected()));
		previdencia2020 = CalculadoraDeSalario.calculaPrevidencia(new  Salario2017(dadosEncontrados.get(2).getSalario(),constante2020,cbCarreira.isSelected()));

		double diferenca = previdencia2020 - previdencia2017;
		double percentagem = (diferenca/previdencia2017);

		resultado = "       No ano de 2018 para 2020 houve um aumento da contribuicao de : ";
		String diferencaFormatada = df.format(diferenca);
		String percentagemFormatada = "\n O que corresponde um percentual de : "+df2.format(percentagem);
		lPrevidencia.setText("       Previdencia 2018: "+df.format(previdencia2017)+"                                                            Previdencia 2019: "+df.format(previdencia2019)+
				"                                                            Previdencia 2020: "+df.format(previdencia2020));
		lResultado.setText(resultado+diferencaFormatada+percentagemFormatada);
	}
	public void setData(List<Carreira> lista) {

		this.lista = lista;


	}

	public ArrayList<Carreira> encontraCarreira(String nome, String tipo, String titulacao) {


		ArrayList<Carreira> dadosCarreira = new ArrayList<Carreira>();

		for(Carreira c: this.lista) {
			String no = c.getClasse() + " - " + c.getNivel();
			if(boxCarreiras.getSelectedIndex()==12){
				if(no.equals("TITULAR  - 1") && c.getTipo().equals(tipo) && c.getTitulacao().equals(titulacao)){
					dadosCarreira.add(c);
				}
			}
			else if(no.equals(nome) && c.getTipo().equals(tipo) && c.getTitulacao().equals(titulacao)){

				dadosCarreira.add(c);
			}


		}

		return dadosCarreira;

	}	

}
